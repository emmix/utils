
Basics 
=================================

# Go to definition using g
gd will take you to the local declaration.
gD will take you to the global declaration.
g* search for the word under the cursor (like *, but g* on 'rain' will find words like 'rainbow').
g# same as g* but in backward direction.
gg goes to the first line in the buffer (or provide a count before the command for a specific line).
G goes to the last line (or provide a count before the command for a specific line).
gf will go to the file under the cursor
g] and other commands will jump to a tag definition 

# go
https://github.com/fatih/vim-go-tutorial

# copy & paste between different vim instances
select contents in instance A
"+y
switch to instance B
"+p

# open directory of current file
http://vimcasts.org/episodes/the-file-explorer

lazy	mnemonic	    open file explorer
:e.	:edit .	    at current working directory
:sp.	:split .	    in split at current working directory
:vs.	:vsplit .	 in vertical split at current working directory
:E	   :Explore	    at directory of current file
:Se	:Sexplore	 in split at directory of current file
:Vex	:Vexplore	 in vertical split at directory of current file

# switch between .cpp and .h
# '%' is shorthand for current filename, ':r' removes the extension, '.cpp' simply appends that string at the end.
: e %:r.cpp 
: vsplit %:r.cpp
: move cursor to .h file, use "gf" or "ctrl-w + f" to jump into .h file and use 'ctrl_^' to return
gf open the file under the cursor in the same window(goto file)
Ctrl-w f    open in a new horizontal window
Ctrl-w gf   open in a new tab


# switch between most recent two files
ctrl+^

# Open two files side by side using vim
$ vim -O Compositor.h Compositor.cpp

# 'dw' will delete from the current cursor position to the beginning of the next word character
# 'd$' will delete from the current cursor position to the end of the current line. 'D' is a synonym for 'd$'
dw
d$/D

# jump between windows
Ctrl+w w   jump to next window
Ctrl+w h   jump to left window
Ctrl+w l   jump to right window
Ctrl+w j   jump to up window
Ctrl+w k   jump to down window

# resize the window
Ctrl+w >       widens the viewpoint by 1 line
Ctrl+w <       narrows the viewpoint by 1 line
Ctrl+w 10 >    widens the viewpoint by 10 line
Ctrl+w 10 <    narrows the viewpoint by 10 line
Ctrl+w +       increase the height of viewpoint by 1 row
Ctrl+w -       decrease the height of viewpoint by 1 row
10 Ctrl+w +    increase the height of viewpoint by 10 row
10 Ctrl+w -    decrease the height of viewpoint by 10 row
Ctrl+w _       make a viewpoint as high as it can be
10 Ctrl+w _    set the veiwpoint height to specified number of rows

# edit binary
$ vim -b filename
: %!xxd
: edit file and use ":%!xxd -r"
: ":wq" exit
: xxd -r -p ascill_hex.dat > real_hex.dat

# undo and redo
: u
: Ctrl-r

# line mode
Stand on the beginning of the column
Press Ctrl+V, then mark across the column you want to edit.
Press I to insert text at the beginning of the column, A to append text, r to replace highlighted text, d to delete, c to change... etc.
Hit ESC twice when done.

# copy current word
: yaw

# edit last one
$ !vi

# replace 
 (ctrl+v enter)
:%s/
/
/g
:%s/
/\r/g

# search
*/#    == n/shfit-n
:set hlsearch  

# fold/unfold
: help folding
zf%     fold block { }
zo      unfold


# NerdTree
https://github.com/scrooloose/nerdtree

?       help
o       open/close file/dir
t/T     open in the (background)tab, use "gt/gT" switch between tabs
i/gi    open selected file in a split window, 'gi' same as 'i', but leave the cursor on the NERDTree
s/gs    open selected file in a new vsplit, 'gs' same as 's', but leave the cursor on the NERDTree
u       open upper folder
K       jump to first node
J       jump to last node
p/P     jump upper/root folder


# ctag, cscope and auto load cscope.out

#ctag
gd       jump to local definition
ctrl+]   jump to definition
ctrl+t   return
ctrl+o
# cscope
see '~/.vim/plugged/cscope_maps.vim/plugin/cscope_maps.vim' for shor-keys

Files
=================================
# Read .json file
using folding skill

# Read .xml file
using xml.vim plugin


References
=================================
# programmer's vim
http://blog.csdn.net/lhf_tiger/article/details/7216500
https://vimawesome.com/
# quickfix
http://easwy.com/blog/archives/advanced-vim-skills-quickfix-mode/
open: copen


Trouble Shooting
=================================
# paste disorde
http://www.netingcn.com/tag/vim





# shell
Sams - Teach Yourself Shell Programming in 24 Hours.pdf

# share folder between ubuntu and windows on vmware
1. right-click -> local network share -> choose "Share this folder" and "Allow others to ..."
2. sudo smbpasswd -a jfeng 


# nfs
sudo apt-get install nfs-kernel-server nfs-common
sudo service nfs-kernel-server restart
sudo vi /etc/exports
# force nfs re-read export dir
sudo exportfs -a
# verify nfs workable
showmount -e localhost

# tftp
sudo apt-get install tftp tftpd-hpa
sudo vi /etc/default/tftpd-hpa
sudo service tftpd-hpa restart
# verify
tftp localhost -c get Image && ls Image

# awk
https://www.gnu.org/software/gawk/manual/html_node/index.html#SEC_Contents
https://unix.stackexchange.com/questions/120788/pass-shell-variable-as-a-pattern-to-awk

# grep v.s. sed v.s. awk
https://www-users.york.ac.uk/~mijp1/teaching/2nd_year_Comp_Lab/guides/grep_awk_sed.pdf

# ubuntu
open workspace switcher: Super + s
open serch: Super

# gnuplot
Gnuplot in Action, 2nd Edition.pdf
http://www.gnuplot.info/help.html

# tar
tar ztvf a.gz  # previw files
tar zxvf a.gz  # extract files
tar zcvf a.gz ./a  #compress files

# diff
diff -y --suppress-common-lines config.new config.orig 


